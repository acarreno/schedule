//  Created by jsn with love for you.
//
//  ___VARIABLE_moduleName___Presenter.swift
//  Kromasol
//
//  Created by Developer on 2021.
//

import Foundation

class ___VARIABLE_moduleName___Presenter: ___VARIABLE_moduleName___PresenterProtocol {
    
    //MARK: - protocolos
    var view: ___VARIABLE_moduleName___ViewProtocol?
    var interactor: ___VARIABLE_moduleName___InteractorProtocol?
    var router: ___VARIABLE_moduleName___RouterProtocol?

}
