//  Created by jsn with love for you.
//
//  ___VARIABLE_moduleName___Protocols.swift
//  Kromasol
//
//  Created by Developer on 2021.
//

import UIKit

typealias EntryPoint___VARIABLE_moduleName___ = ___VARIABLE_moduleName___ViewProtocol & UIViewController

// MARK: - Router Protocol
protocol ___VARIABLE_moduleName___RouterProtocol {
    var entry: EntryPoint___VARIABLE_moduleName___? { get }
    
    static func createModule() -> ___VARIABLE_moduleName___RouterProtocol
}

// MARK: - View Protocol
protocol ___VARIABLE_moduleName___ViewProtocol {
    var presenter: ___VARIABLE_moduleName___PresenterProtocol? { get set }
    
}

// MARK: - Interactor Protocol
protocol ___VARIABLE_moduleName___InteractorProtocol {
    var presenter: ___VARIABLE_moduleName___PresenterProtocol? { get set }
    
}

// MARK: - Presenter Protocol
protocol ___VARIABLE_moduleName___PresenterProtocol {
    var router: ___VARIABLE_moduleName___RouterProtocol? { get set }
    var interactor: ___VARIABLE_moduleName___InteractorProtocol? { get set }
    var view: ___VARIABLE_moduleName___ViewProtocol? { get set }
    
}
