//  Created by jsn with love for you.
//
//  ___VARIABLE_moduleName___Router.swift
//  Kromasol
//
//  Created by Developer on 2021.
//

import Foundation

class ___VARIABLE_moduleName___Router: ___VARIABLE_moduleName___RouterProtocol {
    var entry: EntryPoint___VARIABLE_moduleName___?
    
    static func createModule() -> ___VARIABLE_moduleName___RouterProtocol {
        let router = ___VARIABLE_moduleName___Router()
        
        // MARK: Assign VIP
        var view: ___VARIABLE_moduleName___ViewProtocol = ___VARIABLE_moduleName___ViewController.instantiate(storyboard: "___VARIABLE_moduleName___View")
        var interactor: ___VARIABLE_moduleName___InteractorProtocol = ___VARIABLE_moduleName___Interactor()
        var presenter: ___VARIABLE_moduleName___PresenterProtocol = ___VARIABLE_moduleName___Presenter()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        
        interactor.presenter = presenter
        
        router.entry = view as? EntryPoint___VARIABLE_moduleName___
        
        return router
    }
     
}
