//
//  WebService.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import Foundation
protocol WebService: class {
    func get(_ urlString: String, completion:@escaping (Result<Any, Error>) -> Void)
    
}
