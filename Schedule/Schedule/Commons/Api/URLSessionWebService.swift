//
//  URLSessionWebService.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import Foundation
final class URLSessionWebService: WebService {
    
    func get(_ urlString: String, completion:@escaping (Result<Any, Error>) -> Void) {
        guard let url = URL(string: urlString) else {
            fatalError("The given urlString is not a valid URL")
        }
        
        let urlRequest = URLRequest(url: url)
        let dataTask = session.dataTask(with: urlRequest) { data, response, error in
            if let error = error {
                completion(.failure(error))
            } else {
                guard let httpResponse = response as? HTTPURLResponse else {
                    fatalError("Unsupported protocol")
                }
                
                if 200 ..< 300 ~= httpResponse.statusCode {
                    if let data = data {
                        completion(.success(data))
                    }
                } else {
                    
                }
            }
        }
        dataTask.resume()
    }
    
    
    let session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    
}
