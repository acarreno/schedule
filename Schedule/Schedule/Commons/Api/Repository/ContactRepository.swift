//
//  ContactRepository.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import Foundation
final class ContactRepository {
    
    private let webService: WebService
    
    init(webService: WebService) {
        self.webService = webService
    }

    func allContacts(completion: @escaping (Result<[ContactModel], Error>) -> Void) {
        webService.get("https://jsonplaceholder.typicode.com/users") { result in
            switch result {
            case .success(let data):
                
                do {
                    let contactList = try JSONDecoder().decode([ContactModel].self, from: data as! Data)
                    completion(.success(contactList))
                } catch {
                    completion(.failure(error))
                }
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
        
    }
    
    func allContactsFrmDB() -> [ContactModel]{
        
        return CoreDataManager().getContactList()
        
    }
    
    func addContactsToBD(contactList: [ContactModel]){
        contactList.forEach { (item) in
            CoreDataManager().createContact(contact: item)
        }
        
    }
}
