//
//  PublicationRepository.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import Foundation
final class PublicationRepository {
    
    private let webService: WebService
    
    init(webService: WebService) {
        self.webService = webService
    }

    func allPublicationsByContact(contact: ContactModel, completion: @escaping (Result<[PublicationModel], Error>) -> Void) {
        webService.get("https://jsonplaceholder.typicode.com/posts?userId=\(contact.id)") { result in
            switch result {
            case .success(let data):
                
                do {
                    let contactList = try JSONDecoder().decode([PublicationModel].self, from: data as! Data)
                    completion(.success(contactList))
                } catch {
                    completion(.failure(error))
                }
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
        
    }
}
