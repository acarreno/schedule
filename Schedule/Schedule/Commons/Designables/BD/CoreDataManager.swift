//
//  CoreDataManager.swift
//  coreDataExample
//
//  Created by iMac on 5/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//


import Foundation
//1
import CoreData

class CoreDataManager {
    //2
    private let container : NSPersistentContainer!
    //3
    init() {
        container = NSPersistentContainer(name: "BD")
        
        setupDatabase()
    }
    
    private func setupDatabase() {
        
            container.loadPersistentStores { (desc, error) in
            if let error = error {
                print("Error loading store \(desc) — \(error)")
                return
            }
            print("Database ready!")
        }
    }
    func createProduct(name:String,ammount:String,cuantity:String, completion: @escaping() -> Void) {
        // 2
        let context = container.viewContext
      
        // 3
        let productEntity = Product(context: context)
        productEntity.ammount=ammount
        productEntity.name=name
        productEntity.cuantity=cuantity
        // 5
        do {
            try context.save()
            //print("Usuario \(history.id!) guardado")
            completion()
        } catch {
         
          print("Error guardando usuario — \(error)")
        }
    }
    
    func getProductList() -> [Product] {
        //1
        let fetchRequest : NSFetchRequest<Product> = Product.fetchRequest()
        do {
      
            //2
            let result = try container.viewContext.fetch(fetchRequest)
            return result
        } catch {
            print("error getting history list \(error)")
         }
     
          //3
         return []
    }
    func deleteProduct(id:UUID, completion: @escaping() -> Void) {
        let context = container.viewContext
        let fetchRequest : NSFetchRequest<Product> = Product.fetchRequest()
          do {
        
          
              let result = try container.viewContext.fetch(fetchRequest)
            for item in result {
                if item.id==id{
                    context.delete(item)
                }
            }
             
          } catch {
              print("error getting history list \(error)")
           }
     
        
    }
}
