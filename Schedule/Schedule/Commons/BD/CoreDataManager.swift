//
//  CoreDataManager.swift
//  coreDataExample
//
//  Created by iMac on 5/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//


import Foundation
//1
import CoreData

class CoreDataManager {
    //2
    private let container : NSPersistentContainer!
    //3
    init() {
        container = NSPersistentContainer(name: "BD")
        
        setupDatabase()
    }
    
    private func setupDatabase() {
        
            container.loadPersistentStores { (desc, error) in
            if let error = error {
                print("Error loading store \(desc) — \(error)")
                return
            }
            print("Database ready!")
        }
    }
    
    func createContact(contact:ContactModel ) {
        // 2
        let context = container.viewContext
      
        // 3
        let contactEntity = ContactEntity(context: context)
        contactEntity.phone = contact.phone
        contactEntity.name = contact.name
        contactEntity.id = Int16(contact.id)
        contactEntity.email = contact.email
        // 5
        do {
            try context.save()
            //print("Usuario \(history.id!) guardado")
            
        } catch {
         
          print("Error guardando usuario — \(error)")
        }
    }
    
    func getContactList() -> [ContactModel] {
        //1
        let fetchRequest : NSFetchRequest<ContactEntity> = ContactEntity.fetchRequest()
        do {
      
            //2
            let result = try container.viewContext.fetch(fetchRequest)
            if result.count > 0 {
                
                let jsonArray = convertToJSONArray(moArray: result)
                
                return jsonArray.map { (item) -> ContactModel in
                    return try! DictionaryDecoder().decode(ContactModel.self, from: (item) )
                }
                
            }else{
                return []
            }
           
            
        } catch {
            print("error getting history list \(error)")
         }
     
          //3
         return []
    }
    

    func convertToJSONArray(moArray: [NSManagedObject]) -> [[String: Any]]{
        var jsonArray: [[String: Any]] = []
        for item in moArray {
            var dict: [String: Any] = [:]
            for attribute in item.entity.attributesByName {
                //check if value is present, then add key to dictionary so as to avoid the nil value crash
                if let value = item.value(forKey: attribute.key) {
                    dict[attribute.key] = value
                }
            }
            jsonArray.append(dict)
        }
        return jsonArray
    }

}
