//
//  ContactDetailViewController.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import UIKit

class ContactDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    typealias CellModel =  (cellInfo:ContactDetailGetCellInfo, height: Int)
    
    var container: Array<CellModel> = Array()
    var contactDetail: ContactModel!
    var presenter: ViewToPresenterContactDetailProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initFunctions()
        
    }
    
    /**this functions initialize all methods*/
    func initFunctions(){
        
        startReturnContatc()
        startFechingContactDetail()
        registerCells()
        
        
    }
    
    func inflateTable(publications: [PublicationModel]){
        
        container = []
       
        container.append((
            cellInfo:.cellDefaultInfo(idCell: .contactInformation, idNib: .contactInformation, obj: contactDetail),
            height: 265
        ))
        
        publications.forEach{ container.append((
            cellInfo:.cellDefaultInfo(idCell: .publication, idNib: .publication, obj: $0),
            height: calculateHeightPublicationCell(obj: $0)
        ))
        
        }
       
        tableView.reloadData()
        
    }
    /** here register (Nibs) for before use the table*/
    func registerCells(){
        
        tableView.register(UINib(nibName: ContactDetailNibIdsEnums.contactInformation.rawValue, bundle: nil), forCellReuseIdentifier: ContactDetailCellsIdsEnums.contactInformation.rawValue)
        tableView.register(UINib(nibName: ContactDetailNibIdsEnums.publication.rawValue, bundle: nil), forCellReuseIdentifier: ContactDetailCellsIdsEnums.publication.rawValue)
       
    }
    
    func startFechingContactDetail(){
        
        presenter?.startFetchingPublications()
        
    }
    
    func startReturnContatc(){
        
        presenter?.startReturnContact()
        
    }
    func calculatedHeight(for text: String, width: CGFloat) -> CGFloat {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: width,
                                          height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    func calculateHeightPublicationCell(obj: PublicationModel) -> Int{
        print("------------")
        print( obj.body)
        return Int(calculatedHeight(for: obj.body, width: view.frame.width)) + Int(calculatedHeight(for: obj.title, width: view.frame.width)) + 30
        
    }
    /** this function excecute open seeker method*/
    /*func openSeeker() {
        
        presenter?.openSeeker(navigationController: self.navigationController!)
        
    }*/
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ContactDetailViewController: PresenterToViewContactDetailProtocol{
    
    func onReturnContact(contact: ContactModel) {
        contactDetail = contact
    }
    
    
    func onContactDetailResponseSuccess(publicationList: Array<PublicationModel>) {
        
        DispatchQueue.main.async { [weak self] in
            
            self?.inflateTable(publications: publicationList)
            
        }
    }
    
    func onContactDetailResponseFailed(error: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alert", message: "Problem Fetching Publications", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
}
extension ContactDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.row)
        let item = container[indexPath.row]
       
        
        switch item.cellInfo {
        case .cellDefaultInfo(idCell: let idCell, idNib: _ , obj: _):
            
            let cell = tableView.dequeueReusableCell(withIdentifier: idCell.rawValue, for: indexPath) as! ContactDetailTableViewCell
            
            cell.setUp(cellInfo: item.cellInfo)
            
            return cell
        }
        
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return container.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = container[indexPath.row]
        /*print(indexPath.row)
        print(item.height)*/
        return CGFloat(item.height)
    }
    
}
