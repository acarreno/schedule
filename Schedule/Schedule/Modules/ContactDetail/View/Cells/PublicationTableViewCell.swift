//
//  PublicationTableViewCell.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import UIKit

class PublicationTableViewCell: UITableViewCell {

    static let identifier = "PublicationTableViewCell"
    static let nibName = "PublicationCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
