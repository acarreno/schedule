//
//  ContactDetailTableViewCell.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import UIKit

class ContactDetailTableViewCell: UITableViewCell {
    
    static let nibName = "ContactDetailCell"
    
    @IBOutlet weak var lblFirtLetter: UILabel?
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblMail: UILabel?
    @IBOutlet weak var lblPhone: UILabel?
    @IBOutlet weak var lblTitle: UILabel?
    @IBOutlet weak var txtABody: UITextView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func prepareForReuse() {
        
        lblFirtLetter?.text = nil
        lblName?.text = nil
        lblMail?.text = nil
        lblPhone?.text = nil
        lblTitle?.text = nil
        txtABody?.text = nil
    }
    
    func setUp(cellInfo: ContactDetailGetCellInfo){
        switch cellInfo {
        case .cellDefaultInfo(idCell: let idCell, idNib: _, obj: let obj):
            switch idCell {
            case .contactInformation:
                setUpContact(contact: obj as! ContactModel)
            default:
                setUpPublication(publication: obj as! PublicationModel)
            }
        }
    }
    
    func setUpContact(contact: ContactModel){
        
        lblName?.text = contact.name
        lblPhone?.text = "Tel: \(contact.phone)"
        lblMail?.text = "Correo: \(contact.email)"
        lblFirtLetter?.text = "\(contact.name.first!)"
        
    }
    
    func setUpPublication(publication: PublicationModel){
        
        lblTitle?.text = publication.title
        txtABody?.text = publication.body
        
    }
    
    
}
