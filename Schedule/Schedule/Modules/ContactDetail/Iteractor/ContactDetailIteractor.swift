//
//  ContactDetailIteractor.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import Foundation
class ContactDetailInteractor: PresenterToInteractorContactDetailProtocol{
    
    
    var contact: ContactModel?
    weak var presenter: InteractorToPresenterContactDetailProtocol?
    var repository: PublicationRepository
    
    init() {
       
        repository = PublicationRepository(webService: URLSessionWebService())
    }
    
    func fetchPublications() {
        
        repository.allPublicationsByContact(contact: contact!) { (result) in
            switch result{
            case.success(let list):
                
                self.presenter?.publicationListFetchSuccess(publicationList: list)
                
            case .failure(let error):
                
                self.presenter?.publicationListFetchFailed(error: error.localizedDescription)
                
            }
        }
    }
     
    func returnContact() {
        presenter?.returnContactSuccess(contact: contact!)
    }
    
}

