//
//  PublicationModel.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import Foundation
struct PublicationModel: Codable {
    
    var userId: Int
    var id: Int
    var title: String
    var body: String
   
}
