//
//  ContactDetailRouter.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import Foundation
import UIKit

class ContactDetailRouter: PresenterToRouterContactDetailProtocol{
    
    static var storyboard: UIStoryboard{
        
        return UIStoryboard(name:"ContactDetail",bundle: Bundle.main)
        
    }
    
    /** instantiate the ContactDetail´s module*/
    static func createModule(contact: ContactModel) -> ContactDetailViewController {
        
        let view = ContactDetailRouter.storyboard.instantiateViewController(withIdentifier: "ContactDetailViewController") as! ContactDetailViewController
        
        let presenter: ViewToPresenterContactDetailProtocol & InteractorToPresenterContactDetailProtocol = ContactDetailPresenter()
        let interactor: PresenterToInteractorContactDetailProtocol = ContactDetailInteractor()
        let router: PresenterToRouterContactDetailProtocol = ContactDetailRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        interactor.contact = contact
        
        return view
        
    }
    
}
