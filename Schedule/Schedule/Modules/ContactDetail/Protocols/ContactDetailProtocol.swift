//
//  ContactDetailProtocol.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import Foundation
protocol ViewToPresenterContactDetailProtocol : class {
    
    var view: PresenterToViewContactDetailProtocol? {get set}
    var interactor: PresenterToInteractorContactDetailProtocol? {get set}
    var router: PresenterToRouterContactDetailProtocol? {get set}
    func startFetchingPublications()
    func startReturnContact()
}

protocol PresenterToViewContactDetailProtocol: class {
    
    func onReturnContact(contact: ContactModel)
    func onContactDetailResponseSuccess(publicationList: Array<PublicationModel>)
    func onContactDetailResponseFailed(error: String)
    
}

protocol PresenterToRouterContactDetailProtocol: class {
    
    static func createModule(contact: ContactModel)->ContactDetailViewController
    
}

protocol PresenterToInteractorContactDetailProtocol: class {
    
    var presenter: InteractorToPresenterContactDetailProtocol? {get set}
    var contact: ContactModel? {get set}
    func fetchPublications()
    func returnContact()
    
}

protocol InteractorToPresenterContactDetailProtocol: class {
    
    func publicationListFetchSuccess(publicationList: Array<PublicationModel>)
    func returnContactSuccess(contact: ContactModel)
    func publicationListFetchFailed(error: String)
    
    
}
