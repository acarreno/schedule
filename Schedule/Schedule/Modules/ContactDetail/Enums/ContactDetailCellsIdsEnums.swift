//
//  ContactDetailCellsIdsEnums.swift
//  Schedule
//
//  Created by iMac on 11/07/21.
//

import Foundation
enum ContactDetailCellsIdsEnums: String {
    
    case publication = "PublicationCell"
    case contactInformation = "ContactDetailCell"
    
}
enum ContactDetailNibIdsEnums: String {
    
    case publication = "PublicationCell"
    case contactInformation = "ContactDetailCell"
    
}
enum ContactDetailGetCellInfo {
    
    case cellDefaultInfo(idCell:ContactDetailCellsIdsEnums, idNib: ContactDetailNibIdsEnums, obj: Any?)
    
}
