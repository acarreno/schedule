//
//  ContactDetailPresenter.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import Foundation

class ContactDetailPresenter: ViewToPresenterContactDetailProtocol{
    
    var view: PresenterToViewContactDetailProtocol?
    var interactor: PresenterToInteractorContactDetailProtocol?
    var router: PresenterToRouterContactDetailProtocol?
    
    func startFetchingPublications() {
        
        interactor?.fetchPublications()
        
    }
    
    func startReturnContact() {
        
        interactor?.returnContact()
        
    }
    
}
extension ContactDetailPresenter: InteractorToPresenterContactDetailProtocol{
    
    func returnContactSuccess(contact: ContactModel) {
        
        view?.onReturnContact(contact: contact)
        
    }
    
    func publicationListFetchSuccess(publicationList: Array<PublicationModel>) {
        
        view?.onContactDetailResponseSuccess(publicationList: publicationList)
        
    }
    
    func publicationListFetchFailed(error: String) {
        
        view?.onContactDetailResponseFailed(error: error)
        
    }
    
}
