//
//  ContactListProtocols.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import Foundation
import UIKit

protocol ViewToPresenterContactListProtocol : class {
    
    var view: PresenterToViewContactListProtocol? {get set}
    var interactor: PresenterToInteractorContactListProtocol? {get set}
    var router: PresenterToRouterContactListProtocol? {get set}
    func startFetchingContacts()
    func startFilterContacts(contactList: Array<ContactModel>, query: String)
    func showProductDetail(contact: ContactModel, navigationController: UINavigationController)
    
}

protocol PresenterToViewContactListProtocol: class {
    
    func onContactListResponseSuccess(contactList: Array<ContactModel>)
    func onContactListResponseFailed(error: String)
    func onContactListFilterSuccess(contactList: Array<ContactModel>)
    
}

protocol PresenterToRouterContactListProtocol: class {
    
    static func createModule()->ContactListViewController
    func pushContactDetail(contact: ContactModel, navigationController: UINavigationController)
    
}

protocol PresenterToInteractorContactListProtocol: class {
    
    var presenter:InteractorToPresenterContactListProtocol? {get set}
    func fetchContacts()
    func filterContacts(contactList: [ContactModel], query: String)
    
}

protocol InteractorToPresenterContactListProtocol: class {
    
    func contactsListFetchSuccess(contactList: Array<ContactModel>)
    func contactsListFetchFailed(error: String)
    func contactListFilterSuccess(contactList: Array<ContactModel>)
    
}
