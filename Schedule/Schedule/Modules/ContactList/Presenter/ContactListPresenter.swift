//
//  ContactListPresenter.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import Foundation
import UIKit

class ContactListPresenter: ViewToPresenterContactListProtocol{
    
    var view: PresenterToViewContactListProtocol?
    var interactor: PresenterToInteractorContactListProtocol?
    var router: PresenterToRouterContactListProtocol?

    func startFetchingContacts() {
        
        interactor?.fetchContacts()
        
    }
   
    func startFilterContacts(contactList: Array<ContactModel>, query: String) {
        
        interactor?.filterContacts(contactList: contactList, query: query)
        
    }
    
    func showProductDetail(contact: ContactModel, navigationController: UINavigationController) {
        
        router?.pushContactDetail(contact: contact, navigationController: navigationController)
        
    }
   
}

extension ContactListPresenter: InteractorToPresenterContactListProtocol{
    
    func contactListFilterSuccess(contactList: Array<ContactModel>) {
        view?.onContactListFilterSuccess(contactList: contactList)
    }
    
    /**
        in this function fetch products
        :params:: producList - array after inflate collection view
        
     */
    func contactsListFetchSuccess(contactList : Array<ContactModel>) {
        
        let list = orderContactsByName(contactList: contactList)
        
        view?.onContactListResponseSuccess(contactList: list)
        
    }
    
    func orderContactsByName(contactList : Array<ContactModel>) -> [ContactModel]{
        return contactList.sorted{ $0.name < $1.name }
    }
    /**
        in this function get error in petition
        :params:: error - error from api
        
     */
    func contactsListFetchFailed(error: String) {
        
        view?.onContactListResponseFailed(error: error)
    }
    
    
}

/*extension ContactListPresenter: PresenterToPresenterSeekerProtocol{
    /** this function pass data to ContactList presenter*/
    func dissmiss(query: String) {
        print(query)
        startFetchingProducts(query: query)
    }
    
    
}*/
