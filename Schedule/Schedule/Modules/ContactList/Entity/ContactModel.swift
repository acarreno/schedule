//
//  ContactModel.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import Foundation
struct ContactModel: Codable {
    
    var id: Int
    var name: String
    var username: String?
    var email: String
    var phone: String
    var website: String?
   
}
