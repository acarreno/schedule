//
//  ContactListViewController.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import UIKit

class ContactListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewListEmpty: UIView!
    @IBOutlet weak var lblEmpty: UILabel!
    // in container is find all information about products/
    var container: Array<ContactModel> = Array()
    var orgiginalContainer: Array<ContactModel> = Array()
    var presenter: ViewToPresenterContactListProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initFunctions()
        
        // Do any additional setup after loading the view.
    }
    
    /**this functions initialize all methods*/
    func initFunctions(){
        
        startFechingContactList()
        registerCells()
        addTargets()
        addEmptyView()
        
    }
    
    func addEmptyView(){
        
        tableView.backgroundView = viewListEmpty
        
    }
    
    func showListEmpty(){
        
        viewListEmpty.isHidden = false
        tableView.separatorStyle = .none
        
    }
    
    func hideListEmpty(){
        viewListEmpty.isHidden = true
        tableView.separatorStyle = .singleLine
        
    }
    /** here register (Nibs) for before use the table*/
    func registerCells(){
        
        tableView.register(UINib(nibName: "ContactCell", bundle: nil), forCellReuseIdentifier: ContactListViewCell.identifier)
       
    }
    
    func startFechingContactList(){
        presenter?.startFetchingContacts()
    }
    
    func addTargets(){
        txtSearch.addTarget(self, action: #selector(changeFilterText(_:)), for: .editingChanged)
    }
    
    @objc func changeFilterText(_ sender: UITextField){
        
        presenter?.startFilterContacts(contactList: orgiginalContainer, query: sender.text ?? "")
        
    }
    
    func validateCoutContainer(){
        
        (container.count == 0) ? showListEmpty() : hideListEmpty()
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ContactListViewController: PresenterToViewContactListProtocol{
    
    func onContactListFilterSuccess(contactList: Array<ContactModel>) {
        
        container = contactList
        
        DispatchQueue.main.async { [weak self] in
            
            self?.tableView.reloadData()
            
            self?.validateCoutContainer()
            
        }
        
    }
    
    
    func onContactListResponseSuccess(contactList: Array<ContactModel>) {
        
        container = contactList
        orgiginalContainer = contactList
        
        DispatchQueue.main.async {[weak self] in
            
            self?.tableView.reloadData()
            
            self?.validateCoutContainer()
            
        }
        
    }
    
    func onContactListResponseFailed(error: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alert", message: "Problem Fetching Notice", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
}
extension ContactListViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item=container[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ContactListViewCell.identifier, for: indexPath) as! ContactListViewCell
        
        cell.setUp(contact: item)
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return container.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = container[indexPath.row]
        presenter?.showProductDetail(contact: item, navigationController: navigationController!)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        print(indexPath.row)
        return 75
    }
    
}
