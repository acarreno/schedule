//
//  TableViewCell.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import UIKit

class ContactListViewCell: UITableViewCell {

    static let identifier = "ContactListViewCell"

    @IBOutlet weak var lblFirtLetter: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var viewFirtLetter: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func prepareForReuse() {
        /*imgPublication?.image = nil
        lblName?.text = nil
        lblPrice?.text = nil*/
    }
    
    func setUp(contact: ContactModel){
        
        lblName?.text = contact.name
        lblPhone.text = "Tel: \(contact.phone)"
        lblMail.text = "Correo: \(contact.email)"
        lblFirtLetter.text = "\(contact.name.first!)"
        
    }
}
