//
//  ContactListInteractor.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import Foundation
class ContactListInteractor: PresenterToInteractorContactListProtocol{
  
    weak var presenter: InteractorToPresenterContactListProtocol?
    
    var repository: ContactRepository
    
    init() {
       
        repository = ContactRepository(webService: URLSessionWebService())
    }
    
    func fetchContacts() {
        let contacts = repository.allContactsFrmDB()
        if contacts.isEmpty{
            repository.allContacts { (result) in
                
                switch result{
                case.success(let list):
                    
                    self.presenter?.contactsListFetchSuccess(contactList: list)
                    self.repository.addContactsToBD(contactList: list)
                    
                case .failure(let error):
                    
                    self.presenter?.contactsListFetchFailed(error: error.localizedDescription)
                    
                }
                
            }
        }else{
            self.presenter?.contactsListFetchSuccess(contactList: contacts)
        }
        
        
    }
    
    func filterContacts(contactList: [ContactModel], query: String){
        
        var filterList = contactList
        
        if query != ""{
            filterList = contactList.filter{$0.name.lowercased().contains(query.lowercased())}
        }
        
        self.presenter?.contactListFilterSuccess(contactList: filterList)
    }
        
}

/*extension ContactListInteractor: InteractorToServicesProtocols{
    
    func susses(result: [String : Any]) {
        
        //let result = try! DictionaryDecoder().decode(ResultModel.self, from: (result) )
        
        //self.presenter?.productFetchSuccess(producList: result.results ?? [])
       
    }
    
    func failed(error: String) {
        
        presenter?.productFetchFailed(error: error)
        
    }
    
    
}*/
