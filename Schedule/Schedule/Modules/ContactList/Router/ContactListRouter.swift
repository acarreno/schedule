//
//  ContactListMoodel.swift
//  Schedule
//
//  Created by iMac on 10/07/21.
//

import Foundation
import UIKit

class ContactListRouter: PresenterToRouterContactListProtocol{
    
    static var storyboard: UIStoryboard{
        
        return UIStoryboard(name:"ContactList",bundle: Bundle.main)
        
    }
    
    /** instantiate the ContactList´s module*/
    static func createModule() -> ContactListViewController {
        
        let view = ContactListRouter.storyboard.instantiateViewController(withIdentifier: "ContactListViewController") as! ContactListViewController
        
        let presenter: ViewToPresenterContactListProtocol & InteractorToPresenterContactListProtocol = ContactListPresenter()
        let interactor: PresenterToInteractorContactListProtocol = ContactListInteractor()
        let router: PresenterToRouterContactListProtocol = ContactListRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
        
    }
    
    
    /** go to detail Contact*/
    func pushContactDetail(contact: ContactModel, navigationController: UINavigationController) {
        
        let view = ContactDetailRouter.createModule(contact: contact)
        
        navigationController.pushViewController(view, animated: true)
    }
}
