//
//  ContatctDetailTest.swift
//  ScheduleTests
//
//  Created by iMac on 11/07/21.
//

import Foundation
import XCTest
@testable import Schedule

class ContactDetailPresenterTests: XCTestCase {
    
    var mockView: MockView!
    var mockRouter: MockRouter!
    var mockInteractor: MockInteractor!
    var sut: ContactDetailPresenter!
    
    struct DummyData {
        
        static let contactJson = [
            "id": 1,
            "name": "Leanne Graham",
            "username": "Bret",
            "email": "Sincere@april.biz",
            "phone": "1-770-736-8031 x56442"
        ] as [String : Any]
        
        static let contact = try! DictionaryDecoder().decode(ContactModel.self, from: (DummyData.contactJson) )
    }
    
    override func setUp(){
        super.setUp()
        mockView = MockView()
        mockRouter = MockRouter()
        mockInteractor = MockInteractor()
        sut = ContactDetailPresenter()
        sut.view = mockView
        sut.router = mockRouter
        sut.interactor = mockInteractor
    }
    
    override func tearDown() {
        super.tearDown()
        mockView = nil
        mockRouter = nil
        mockInteractor = nil
        sut = nil
    }
    
    func test_startFechingPublications(){
        //Given
        
        //When
        sut.startFetchingPublications()
        //Then
        XCTAssertTrue(mockInteractor.getFetchPublications)
    }
    
    func test_startFilterContacts(){
        //Given
        
        //When
        sut.startReturnContact()
        //Then
        XCTAssertTrue(mockInteractor.getReturnContact)
    }
    
    func test_contactsListFetchFailed(){
        //Given
        
        //When
        sut.publicationListFetchFailed(error: "")
        //Then
        XCTAssertTrue(mockView.getOnContactDetailResponseFailed)
    }
    
    class MockView: PresenterToViewContactDetailProtocol {
        
        
        var getOnReturnContact: Bool = false
        var getOnContactDetailResponseSuccess: Bool = false
        var getOnContactDetailResponseFailed: Bool = false
        
        func onReturnContact(contact: ContactModel) {
            
            getOnReturnContact = true
            
        }
        
        func onContactDetailResponseSuccess(publicationList: Array<PublicationModel>) {
            
            getOnContactDetailResponseSuccess = true
            
        }
        
        func onContactDetailResponseFailed(error: String) {
            
            getOnContactDetailResponseFailed = true
            
        }
        
    }
    
    class MockRouter: PresenterToRouterContactDetailProtocol {
        
        
        
        var getCreateModule: Bool = false
        var getPushContactDetail: Bool = false
        
        static func createModule(contact: ContactModel) -> ContactDetailViewController {
            
            ContactDetailViewController()
            
        }
        
        
    }
    
    class MockInteractor: PresenterToInteractorContactDetailProtocol {
        
        var getFetchPublications: Bool = false
        var getReturnContact: Bool = false
        
        var presenter: InteractorToPresenterContactDetailProtocol?
        var contact: ContactModel? = DummyData.contact
        
        func fetchPublications() {
            
            getFetchPublications = true
            
        }
        
        func returnContact() {
            
            getReturnContact = true
            
        }
        
    }
}

    

