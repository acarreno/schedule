//
//  ContactDetailInteractorTest.swift
//  ScheduleTests
//
//  Created by iMac on 12/07/21.
//

import Foundation
import XCTest
@testable import Schedule

class ContactDetailInteractorTests: XCTestCase {
    
    var mockPresenter: MockPresenter!
    var sut: ContactDetailInteractor!
    
    struct DummyData {
        
        static let contactJson = [
            "id": 1,
            "name": "Leanne Graham",
            "username": "Bret",
            "email": "Sincere@april.biz",
            "phone": "1-770-736-8031 x56442"
        ] as [String : Any]
        
        static let contact = try! DictionaryDecoder().decode(ContactModel.self, from: (DummyData.contactJson) )
    }
    
    override func setUp(){
        super.setUp()
        mockPresenter = MockPresenter()
        sut = ContactDetailInteractor()
        
        sut.presenter = mockPresenter
    }
    
    override func tearDown() {
        super.tearDown()
        
        mockPresenter = nil
        sut = nil
        
    }
    
    func test_fetchPublications(){
        //Given
        mockPresenter.expectation = expectation(description: #function)
        //When
        sut.contact = DummyData.contact
        sut.fetchPublications()
        //Then
        wait(for: [mockPresenter.expectation], timeout: 6)
        XCTAssertTrue(mockPresenter.publicationListFetchSuccessCalled)
        
    }
    
    func test_fetchContacts(){
        //Given
        
        //When
        sut.contact = DummyData.contact
        sut.returnContact()
        //Then
        XCTAssertTrue(mockPresenter.returnContactSuccessCalled)
        
    }
    
    func test_fetchProductsFailed(){
        //Given
        mockPresenter.expectation = expectation(description: #function)
        //When
        sut.contact = DummyData.contact
        sut.fetchPublications()
        //Then
        wait(for: [mockPresenter.expectation], timeout: 6)
        XCTAssertTrue(mockPresenter.publicationListFetchSuccessCalled)
        
    }
    
    class MockPresenter: InteractorToPresenterContactDetailProtocol {
        
        var publicationListFetchSuccessCalled: Bool = false
        var returnContactSuccessCalled: Bool = false
        var publicationListFetchFailedCalled: Bool = false
        var expectation: XCTestExpectation!
        
        func publicationListFetchSuccess(publicationList: Array<PublicationModel>) {
            
            publicationListFetchSuccessCalled = true
            expectation.fulfill()
            
        }
        
        func returnContactSuccess(contact: ContactModel) {
            returnContactSuccessCalled = true
        }
        
        func publicationListFetchFailed(error: String) {
            publicationListFetchSuccessCalled = true
            expectation.fulfill()
        }
    }
    
    
}
