//
//  ContactListTest.swift
//  ScheduleTests
//
//  Created by iMac on 11/07/21.
//

import Foundation
import XCTest
@testable import Schedule

class ContactListPresenterTests: XCTestCase {
    
    var mockView: MockView!
    var mockRouter: MockRouter!
    var mockInteractor: MockInteractor!
    var sut: ContactListPresenter!
    
    struct DummyData {
        
        static let contactJson = [
            "id": 1,
            "name": "Leanne Graham",
            "username": "Bret",
            "email": "Sincere@april.biz",
            "phone": "1-770-736-8031 x56442"
        ] as [String : Any]
        
        static let contact = try! DictionaryDecoder().decode(ContactModel.self, from: (DummyData.contactJson) )
    }
    
    override func setUp(){
        super.setUp()
        mockView = MockView()
        mockRouter = MockRouter()
        mockInteractor = MockInteractor()
        sut = ContactListPresenter()
        sut.view = mockView
        sut.router = mockRouter
        sut.interactor = mockInteractor
    }
    
    override func tearDown() {
        super.tearDown()
        mockView = nil
        mockRouter = nil
        mockInteractor = nil
        sut = nil
    }
    
    func test_startFechingProducts(){
        //Given
        
        //When
        sut.startFetchingContacts()
        //Then
        XCTAssertTrue(mockInteractor.getFetchContacts)
    }
    
    func test_startFilterContacts(){
        //Given
        
        //When
        sut.startFilterContacts(contactList: [], query: "")
        //Then
        XCTAssertTrue(mockInteractor.getFilterContacts)
    }
    
    func test_contactsListFetchSuccess(){
        //Given
        
        //When
        sut.contactsListFetchSuccess(contactList: [])
        //Then
        XCTAssertTrue(mockView.getOnContactListResponseSuccess)
    }
    
    func test_contactsListFetchFailed(){
        //Given
        
        //When
        sut.contactsListFetchFailed(error: "")
        //Then
        XCTAssertTrue(mockView.getOnContactListResponseFailed)
    }
    
    func test_showProducDetail(){
        //Given
        
        //When
        sut.showProductDetail(contact: DummyData.contact, navigationController: UINavigationController())
        //Then
        XCTAssertTrue(mockRouter.getPushContactDetail)
    }
    
    class MockView: PresenterToViewContactListProtocol {
        
        var getOnContactListResponseSuccess: Bool = false
        var getOnContactListResponseFailed: Bool = false
        var getonContactListFilterSuccess: Bool = false
        
        func onContactListResponseSuccess(contactList: Array<ContactModel>) {
            
            getOnContactListResponseSuccess = true
            
        }
        
        func onContactListResponseFailed(error: String) {
            
            getOnContactListResponseFailed = true
            
        }
        
        func onContactListFilterSuccess(contactList: Array<ContactModel>) {
            
            getonContactListFilterSuccess = true
            
        }
        
        
    }
    
    class MockRouter: PresenterToRouterContactListProtocol {
        
        var getCreateModule: Bool = false
        var getPushContactDetail: Bool = false
        
        static func createModule() -> ContactListViewController {
            
            ContactListViewController()
            
        }
        
        func pushContactDetail(contact: ContactModel, navigationController: UINavigationController) {
            
            getPushContactDetail = true
            
        }
        
        
    }
    
    class MockInteractor: PresenterToInteractorContactListProtocol {
        
        var presenter: InteractorToPresenterContactListProtocol?
        var getFetchContacts: Bool = false
        var getFilterContacts: Bool = false
        
        func fetchContacts() {
            
            getFetchContacts = true
            
        }
        
        func filterContacts(contactList: [ContactModel], query: String) {
            
            getFilterContacts = true
            
        }
        
    }
}
