//
//  ContactListInteractorTest.swift
//  ScheduleTests
//
//  Created by iMac on 11/07/21.
//

import Foundation
import XCTest
@testable import Schedule

class ContactListInteractorTests: XCTestCase {
    
    var mockPresenter: MockPresenter!
    var sut: ContactListInteractor!
    
    override func setUp(){
        super.setUp()
        mockPresenter = MockPresenter()
        sut = ContactListInteractor()
        
        sut.presenter = mockPresenter
    }
    
    override func tearDown() {
        super.tearDown()
        
        mockPresenter = nil
        sut = nil
        
    }
    
    func test_fetchContacts(){
        //Given
        mockPresenter.expectation = expectation(description: #function)
        //When
        sut.fetchContacts()
        //Then
        wait(for: [mockPresenter.expectation], timeout: 6)
        XCTAssertTrue(mockPresenter.contactListFetchSuccessCalled)
        
    }
    
    func test_fetchFilterContacts(){
        //Given
        
        //When
        sut.filterContacts(contactList: [], query: "cc")
        //Then
        
        XCTAssertTrue(mockPresenter.contactListFilterSuccessCalled)
        
    }
    
    func test_fetchProductsFailed(){
        //Given
        mockPresenter.expectation = expectation(description: #function)
        //When
        sut.fetchContacts()
        //Then
        wait(for: [mockPresenter.expectation], timeout: 6)
        XCTAssertTrue(mockPresenter.contactListFetchSuccessCalled)
        
    }
    
    class MockPresenter: InteractorToPresenterContactListProtocol {
        
        
        
        var contactListFetchSuccessCalled: Bool = false
        var contactListFilterSuccessCalled: Bool = false
        var contactsListFetchFailedCalled: Bool = false
        var expectation: XCTestExpectation!
        
        func contactsListFetchSuccess(contactList: Array<ContactModel>) {
            
            contactListFetchSuccessCalled = true
            expectation.fulfill()
            
        }
        
        func contactsListFetchFailed(error: String) {
            contactsListFetchFailedCalled = true
        }
        
        func contactListFilterSuccess(contactList: Array<ContactModel>) {
            
            contactListFilterSuccessCalled = true
            
        }
    }
    
    
}
